﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Autoferretaplicacionventas
{
    public class cliente
    {
        public int id_cliente { get; set; }
        public string nombre { get; set; }
        public string nit { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string producto { get; set; }
        public int cantidad { get; set; }
        public double total { get; set; }
        

        public cliente() { }
        public cliente(int id_clientes,string nombre, string nit, string direccion, string telefono, string producto, int cantidad, double total)
        {
            this.id_cliente = id_clientes;
            this.nombre = nombre;
            this.nit = nit;
            this.direccion = direccion;
            this.telefono = telefono;
            this.producto = producto;
            this.cantidad = cantidad;
            this.total = total;
        }
    }
}
