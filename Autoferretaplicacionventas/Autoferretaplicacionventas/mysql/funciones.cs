﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;



namespace Autoferretaplicacionventas
{
    class funciones
    {
        public static int agregar(cliente add)
        {
            int retorno = 0;
              MySqlCommand comando = new MySqlCommand(string.Format("insert into ventas(FK_id_cliente,nombre,nit,direccion,telefono,producto,cantidad,total_a_pagar)value({0},'{1}','{2}','{3}','{4}','{5}',{6}, {7})",add.id_cliente,add.nombre,add.nit,add.direccion,add.telefono,add.producto,add.cantidad,add.total),conexion.obtenerConexion());
              retorno = comando.ExecuteNonQuery();
            return retorno;
        }
        public static List<cliente> mostrar()
        {
            List<cliente> lista = new List<cliente>();
            MySqlCommand comando = new MySqlCommand(String.Format("Select * from ventas"), conexion.obtenerConexion());
            MySqlDataReader reader = comando.ExecuteReader();
            while (reader.Read())
            {
                cliente a = new cliente();
                a.id_cliente = reader.GetInt32(1);
                a.nombre = reader.GetString(2);
                a.nit = reader.GetString(3);
                a.direccion = reader.GetString(4);
                a.telefono = reader.GetString(5);
                a.producto = reader.GetString(6);
                a.cantidad = reader.GetInt32(7);
                a.total = reader.GetDouble(8);
                lista.Add(a);
            }
            return lista;
        }
    }
}
