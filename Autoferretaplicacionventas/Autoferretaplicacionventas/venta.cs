﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Autoferretaplicacionventas
{
    public partial class venta : Form
    {
        public venta()
        {
            InitializeComponent();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Form1 menu = new Form1();
            menu.Show();
            this.Hide();
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtfkidcliente.Text == "" || txtnombre.Text == "" || txtnit.Text == "" || txtdireccion.Text == "" || txtproducto.Text == "" || txttelefono.Text == "" || txtcantidad.Text == "" || txtproducto.Text == "" || txttotal.Text == "")
            {
                MessageBox.Show("Datos vacios, revise las casillas");
            }
            else
            {
                conexion.obtenerConexion();
                cliente agregar = new cliente();
                agregar.id_cliente = Int32.Parse((txtfkidcliente.Text));
                agregar.nombre = txtnombre.Text;
                agregar.nit = txtnit.Text;
                agregar.direccion = txtdireccion.Text;
                agregar.telefono = txttelefono.Text;
                agregar.producto = txtproducto.Text;
                agregar.cantidad = Int32.Parse((txtcantidad.Text));
                agregar.total = Convert.ToDouble((txttotal.Text));
                int retorno = funciones.agregar(agregar);
                if (retorno > 0)
                {
                    MessageBox.Show("Se agrego la venta correctamente");
                }
                else
                {
                    MessageBox.Show("No se pudo agregar la venta");
                }
            }
        }

        private void venta_Load(object sender, EventArgs e)
        {
            dgv.DataSource = funciones.mostrar();
            BTNDELATE.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dgv.DataSource = funciones.mostrar();
        }

        

        private void BTNSELECT_Click(object sender, EventArgs e)
        {
            if(dgv.SelectedRows.Count == 1)
            {
                MessageBox.Show("Fila seleccionada");
                BTNDELATE.Enabled = true;
            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila");
            }
        }

        private void BTNDELATE_Click(object sender, EventArgs e)
        {

        }
    }
}
